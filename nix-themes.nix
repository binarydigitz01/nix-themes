{}:
let
  cfg = config.themes;
   in
{
  options.themes = {
    theme = mkOption {
      type = types.path;
      description = ''
        Path to theme.
'';
    };
  }
  options.themes.emacs = mkOption {
      type = types.bool;
      default = false;
      description = ''
        If enabled, nix-themes will place a `system-theme.el` in
        $XDG_CONFIG_HOME/emacs
      '';
  };

  config = {
    xdg.configFile."emacs/system-theme.el".source = mkIf cfg.emacs cfg.theme + /emacs/system-theme.el;
  }
}
